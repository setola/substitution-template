<?php

/**
 * Stores the definition for class SubstitutionTemplate
 */

namespace TSW\SubstitutionTemplate;

use Exception;

/**
 * Manages a string substitution set
 * @author etessore
 * @version 1.0.0
 * @package classes
 */
class SubstitutionTemplate {

	/**
	 * @var array Stores some static html
	 */
	public $staticMarkupParts;

	/**
	 * @var string the substitution template
	 */
	public $tpl;

	/**
	 * @var string stores the rendered markup
	 */
	protected $render;

	/**
	 * Initializes the current object with default parameters
	 */
	public function __construct($tpl = '') {
		$this->setTemplate($tpl);
	}

	public function minifyTemplate() {
		$this->tpl = str_replace(array("\n", "\r"), '', $this->tpl);
		$this->tpl = preg_replace('@ {2,}@', ' ', $this->tpl);

		return $this;
	}

	/**
	 * Sets the substitutions template
	 *
	 * @param string $tpl the template
	 *
	 * @return SubstitutionTemplate $this for chainability
	 */
	public function setTemplate($tpl) {
		$this->tpl = $tpl;

		return $this;
	}

	/**
	 * Retrieves the markup for the given key
	 * It returns false if the key isn't defined
	 *
	 * @param string $key key to be searched
	 *
	 * @return SubstitutionTemplate $this for chainability
	 */
	public function getMarkupPart($key) {
		if(isset($this->staticMarkupParts[ $key ])) {
			return $this->staticMarkupParts[ $key ];
		}

		return false;
	}

	/**
	 * Set the static markup; ie: prev\next\loading divs
	 *
	 * @param string|array $key the searches to be substituted
	 * @param string|array $markup html markups
	 *
	 * @return SubstitutionTemplate $this for chainability
	 * @throws Exception if $key and $markup have different number of elements
	 */
	public function setMarkupPart($key, $markup) {
		$key    = (array) $key;
		$markup = (array) $markup;

		if(count($markup) == 1 && count($key) > 1) {
			foreach($key as $k => $v) {
				$this->staticMarkupParts[ $v ] = $markup[0];
			}
		} elseif(count($markup) != count($key)) {
			throw new Exception('$key and $markup have different number of elements - KEY: ' . $key . ' - VALUE: ' . var_export($markup));
		} else {
			foreach($key as $k => $v) {
				$this->staticMarkupParts[ $v ] = $markup[ $k ];
			}
		}

		//$this->static_markup[$key] = $markup;
		return $this;
	}

	/**
	 * Bulk set the key:markup pairs
	 *
	 * @param array $ass_array an associative array of keys and markups
	 *
	 * @return SubstitutionTemplate $this for chainability
	 */
	public function setMarkupParts($ass_array) {
		$ass_array = (array) $ass_array;
		$this->setMarkupPart(array_keys($ass_array), array_values($ass_array));

		return $this;
	}

	/**
	 * Replaces the markup in $this->tpl %tag%s with the one
	 * in the corresponding value of $this->static_markup[tag].
	 */
	protected function render() {
		if( ! is_array($this->staticMarkupParts)) {
			$this->staticMarkupParts = array();
		}
		if(empty($this->render)) {
			$this->render = str_replace(
				array_map(
					create_function('$k', 'return "%".$k."%";'),
					array_keys($this->staticMarkupParts)
				),
				array_values($this->staticMarkupParts),
				$this->tpl
			);
		}

		return $this->render;
	}

	/**
	 * Resets the rendered markup
	 * @return SubstitutionTemplate $this for chainability
	 */
	public function reset() {
		$this->render = null;

		return $this;
	}

	/**
	 * Stringify the current object
	 * @return string
	 */
	public function __toString() {
		return $this->render();
	}

	/**
	 * Gets the rendered markup
	 * @return string the markup
	 */
	public function getMarkup() {
		return $this->render();
	}

	/**
	 * Prints the rendered markup
	 */
	public function theMarkup() {
		echo $this->render();
	}
}